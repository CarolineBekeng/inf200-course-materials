import pytest
# For information on parametrize, see
#   https://docs.pytest.org/en/7.1.x/how-to/parametrize.html#parametrize-basics

from fraction import Fraction


@pytest.mark.parametrize('num, den',
                         [(2, 3),
                          (2., 3.),
                          (4, 1),
                          (0, 1),
                          (-5, 12)])
def test_create_legal(num, den):
    Fraction(num, den)


@pytest.mark.parametrize('num, den',
                         [(2, 0),
                          (0, 0),
                          (0, -1),
                          (5, -12),
                          (3.2, 2),
                          (2, 3.1)])
def test_create_illegal(num, den):
    with pytest.raises(ValueError):
        Fraction(num, den)


@pytest.mark.parametrize('x, y',
                         [(Fraction(1, 2), Fraction(1, 2)),
                          (Fraction(1, 2), Fraction(2, 4)),
                          (2, Fraction(4, 2)),
                          (Fraction(9, 3), 3),
                          (-Fraction(4, 5), Fraction(-4, 5)),
                          (-Fraction(-4, 5), Fraction(4, 5)),
                          (Fraction(2, 3), Fraction(2., 3.))
                          ])
def test_eq(x, y):
    assert x == y


@pytest.mark.parametrize('x, y',
                         [(Fraction(3, 2), Fraction(2, 4)),
                          (2, Fraction(2, 4)),
                          (Fraction(3, 9), 3),
                          (Fraction(4, 5), 0),
                          ])
def test_neq(x, y):
    assert x != y


@pytest.mark.parametrize('x, y, res',
                         [(Fraction(1, 2), Fraction(2, 3), Fraction(7, 6)),
                          (Fraction(-1, 2), Fraction(2, 3), Fraction(1, 6)),
                          (Fraction(1, 2), 1, Fraction(3, 2)),
                          (1, Fraction(1, 2), Fraction(3, 2)),
                          ])
def test_add(x, y, res):
    assert x + y == res


@pytest.mark.parametrize('x, y, res',
                         [(Fraction(1, 2), Fraction(2, 3), Fraction(-1, 6)),
                          (Fraction(-1, 2), Fraction(2, 3), Fraction(-7, 6)),
                          (Fraction(1, 2), 1, Fraction(-1, 2)),
                          (1, Fraction(1, 2), Fraction(1, 2)),
                          ])
def test_sub(x, y, res):
    assert x - y == res


@pytest.mark.parametrize('x, y, res',
                         [(Fraction(1, 2), Fraction(2, 3), Fraction(1, 3)),
                          (Fraction(-1, 2), Fraction(2, 3), Fraction(-1, 3)),
                          (Fraction(1, 2), 1, Fraction(1, 2)),
                          (1, Fraction(1, 2), Fraction(1, 2)),
                          (Fraction(5, 7), Fraction(2, 3), Fraction(10, 21)),
                          (0, Fraction(1, 2), Fraction(0, 1))
                          ])
def test_mul(x, y, res):
    assert x * y == res


@pytest.mark.parametrize('x, y, res',
                         [(Fraction(1, 2), Fraction(2, 3), Fraction(3, 4)),
                          (Fraction(-1, 2), Fraction(2, 3), Fraction(-3, 4)),
                          (Fraction(1, 2), 1, Fraction(1, 2)),
                          (1, Fraction(1, 2), Fraction(2, 1)),
                          (Fraction(5, 7), Fraction(2, 3), Fraction(15, 14)),
                          (0, Fraction(1, 2), Fraction(0, 1))
                          ])
def test_div(x, y, res):
    assert x / y == res


@pytest.mark.parametrize('x, y, res',
                         [(Fraction(-1, 2), 2, Fraction(1, 4)),
                          (Fraction(2, 3), -1, Fraction(3, 2)),
                          (Fraction(2, 3), 2., Fraction(4, 9)),
                          (Fraction(2, 3), 0, Fraction(1, 1)),
                          ])
def test_pow(x, y, res):
    assert x**y == res


@pytest.mark.parametrize('x, y',
                         [(Fraction(1, 2), Fraction(2, 1)),
                          (Fraction(1, 2), 1.5)
                          ])
def test_pow_error(x, y):
    with pytest.raises(ValueError):
        x**y


# noinspection DuplicatedCode
@pytest.mark.parametrize('x, y',
                         [(Fraction(2, 3), Fraction(4, 5)),
                          (Fraction(-2, 3), Fraction(-1, 3)),
                          (Fraction(-2, 3), 0),
                          (1, Fraction(5, 3)),
                          (Fraction(2, 3), 0.7)
                          ])
def test_less_than(x, y):
    assert x < y


@pytest.mark.parametrize('x, y',
                         [(Fraction(2, 3), Fraction(4, 5)),
                          (Fraction(-2, 3), Fraction(-1, 3)),
                          (Fraction(-2, 3), 0),
                          (1, Fraction(5, 3)),
                          (Fraction(2, 3), 0.7)
                          ])
def test_greater_than(x, y):
    assert y > x


@pytest.mark.parametrize('x, y',
                         [(Fraction(2, 3), Fraction(4, 5)),
                          (Fraction(2, 3), Fraction(2, 3))
                          ])
def test_less_equal(x, y):
    assert x <= y


@pytest.mark.parametrize('x, y',
                         [(Fraction(2, 3), Fraction(4, 5)),
                          (Fraction(2, 3), Fraction(2, 3))
                          ])
def test_greater_equal(x, y):
    assert y >= x


def test_equal_is_not_less():
    assert not Fraction(2, 3) < Fraction(2, 3)


def test_equal_is_not_greater():
    assert not Fraction(2, 3) > Fraction(2, 3)


def test_str():
    assert str(Fraction(-2, 3)) == "-2/3"


def test_repr():
    assert repr(Fraction(-2, 3)) == "Fraction(-2, 3)"
