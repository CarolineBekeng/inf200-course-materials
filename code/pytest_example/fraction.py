class Fraction:
    def __init__(self, numerator, denominator):

        self.numer = int(numerator)
        self.denom = int(denominator)

        if self.numer != numerator:
            raise ValueError("Numerator must have integer value.")
        if self.denom != denominator:
            raise ValueError("Denominator must have integer value.")
        if self.denom <= 0:
            raise ValueError("Denominator must be > 0.")

    def __str__(self):
        return f"{self.numer}/{self.denom}"

    def __repr__(self):
        return f"Fraction({self.numer}, {self.denom})"

    def __eq__(self, rhs):
        try:
            return self.numer * rhs.denom == rhs.numer * self.denom
        except AttributeError:
            return self == Fraction(rhs, 1)
    
    def __ne__(self, rhs):
        return not self == rhs
    
    def __lt__(self, rhs):
        try:
            return self.numer * rhs.denom < rhs.numer * self.denom
        except AttributeError:
            return self.numer < rhs * self.denom

    def __le__(self, rhs):
        return self < rhs or self == rhs
    
    def __gt__(self, rhs):
        try:
            return self.numer * rhs.denom > rhs.numer * self.denom
        except AttributeError:
            return self.numer > rhs * self.denom

    def __ge__(self, rhs):
        return rhs <= self

    def __neg__(self):
        return Fraction(-self.numer, self.denom)
    
    def __add__(self, rhs):
        try:
            return Fraction(self.numer * rhs.denom + self.denom * rhs.numer, self.denom * rhs.denom)
        except AttributeError:
            return self + Fraction(rhs, 1)

    def __radd__(self, lhs):
        return self + lhs

    def __sub__(self, rhs):
        return self + (-rhs)

    def __rsub__(self, lhs):
        return -self + lhs

    def __mul__(self, rhs):
        try:
            return Fraction(self.numer * rhs.numer, self.denom * rhs.denom)
        except AttributeError:
            return self * Fraction(rhs, 1)

    def __rmul__(self, lhs):
        return self * lhs

    def __truediv__(self, rhs):
        try:
            return Fraction(self.numer * rhs.denom, self.denom * rhs.numer)
        except AttributeError:
            return self / Fraction(rhs, 1)

    def __rtruediv__(self, lhs):
        return Fraction(lhs, 1) / self

    def __pow__(self, rhs):
        try:
            if rhs != int(rhs):
                raise ValueError("Exponent must be an integer.")
        except TypeError:
            raise ValueError("Exponent must be an integer.")

        rhs = int(rhs)
        if rhs > 0:
            return Fraction(self.numer**rhs, self.denom**rhs)
        elif rhs == 0:
            return Fraction(1, 1)
        else:
            return (1 / self)**(-rhs)
